/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia_201502;

import ia_201502.algoritmos.AStar;
import ia_201502.algoritmos.GBFS;
import ia_201502.algoritmos.HillClimbing;
import ia_201502.algoritmos.SimulatedAnnealing;
import ia_201502.impresoras.ImpresorConsola;
import ia_201502.puzzle.Puzzle;
import ia_201502.puzzle.PuzzleAI;

public class Main {
    public static void main(String[] args) {
        Puzzle puzzle = new Puzzle();
        int[][] estadoInicial = {
            {0, 1, 3},
            {8, 2, 4},
            {7, 6, 5}};
        int[][] estadoFinal = {
            {1, 2, 3},
            {8, 0, 4},
            {7, 6, 5}};
        puzzle.setEstado(estadoInicial);
        
        System.out.println("---------------------------------");
        
        System.out.println("Dificultad 1");

        PuzzleAI puzzleIA = new PuzzleAI(puzzle, estadoFinal, new GBFS(puzzle, estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();

        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new AStar(puzzle,estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();

        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new HillClimbing(puzzle,estadoFinal), new ImpresorConsola());
        puzzleIA.resolver(); 
        
        System.out.println("---------------------------------");
        
        System.out.println("Dificultad 2");
        
        estadoInicial = new int[][] {
            {1, 3, 4},
            {8, 0, 5},
            {7, 2, 6}
        };
        
        estadoFinal = new int[][] {
            {1, 2, 3},
            {8, 0, 4},
            {7, 6, 5}
        };
        puzzle.setEstado(estadoInicial);
        
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new GBFS(puzzle, estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();

        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new AStar(puzzle,estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();

        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new HillClimbing(puzzle,estadoFinal), new ImpresorConsola());
        puzzleIA.resolver(); 
        
        System.out.println("---------------------------------");
        
        System.out.println("Dificultad 3");
        
        estadoInicial = new int[][] {
            {1, 2, 3},
            {6, 5, 4},
            {7, 8, 0}
        };
        
        estadoFinal = new int[][] {
            {1, 2, 3},
            {8, 0, 4},
            {7, 6, 5}
        };
        puzzle.setEstado(estadoInicial);
        
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new GBFS(puzzle, estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();

        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new AStar(puzzle,estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();

        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new HillClimbing(puzzle,estadoFinal), new ImpresorConsola());
        puzzleIA.resolver(); 
        
        System.out.println("---------------------------------");
        
        System.out.println("Dificultad 4");
        
        estadoInicial = new int[][] {
            {8, 7, 0},
            {1, 5, 6},
            {2, 3, 4}
        };
        
        estadoFinal = new int[][] {
            {1, 2, 3},
            {8, 0, 4},
            {7, 6, 5}
        };
        puzzle.setEstado(estadoInicial);
        
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new GBFS(puzzle, estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();

        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new AStar(puzzle,estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();

        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new HillClimbing(puzzle,estadoFinal), new ImpresorConsola());
        puzzleIA.resolver(); 
        
        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle,estadoFinal,new SimulatedAnnealing(puzzle, estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();
        
        System.out.println("---------------------------------");
        System.out.println("Dificultad M�xima");
        
        estadoInicial = new int[][] {
            {8, 7, 6},
            {0, 4, 1},
            {2, 5, 3}
        };
        
        estadoFinal = new int[][] {
            {0, 1, 2},
            {3, 4, 5},
            {6, 7, 8}
        };
        puzzle.setEstado(estadoInicial);
        
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new GBFS(puzzle, estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();

        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new AStar(puzzle,estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();

        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle, estadoFinal, new HillClimbing(puzzle,estadoFinal), new ImpresorConsola());
        puzzleIA.resolver(); 
        
        puzzle.setEstado(estadoInicial);
        puzzleIA = new PuzzleAI(puzzle,estadoFinal,new SimulatedAnnealing(puzzle, estadoFinal), new ImpresorConsola());
        puzzleIA.resolver();
        
    }
}
