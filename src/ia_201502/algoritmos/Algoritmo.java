package ia_201502.algoritmos;

import ia_201502.estructuras.Reporte;
import ia_201502.puzzle.Puzzle;
import java.util.List;

public interface Algoritmo {
    public void correr();
    public Reporte getReporte();
}
