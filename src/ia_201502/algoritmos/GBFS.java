package ia_201502.algoritmos;

import ia_201502.estructuras.Reporte;
import ia_201502.algoritmos.Algoritmo;
import ia_201502.estructuras.Nodo;
import ia_201502.puzzle.Puzzle;
import ia_201502.util.MapUtil;
import ia_201502.util.MatrizUtil;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.PriorityQueue;
import objectexplorer.MemoryMeasurer;

public class GBFS implements Algoritmo {
    
    Integer nodosGenerados = 0;
    Long nodosEnRam = new Long(0);
    boolean encuentraSolucion = false;
    Double tiempo = 0.0;
    int[][] estadoFinal;
    int[][] estadoInicial;
    Puzzle puzzle;
    List<List<Integer>> resultado;
    

    public GBFS(Puzzle puzzle, int[][] estadoFinal) {
        this.estadoFinal = estadoFinal;
        this.estadoInicial = puzzle.getEstado();
        this.puzzle = puzzle;
    }
       
    
    @Override
    public void correr() {
        long tiempoInicio = System.nanoTime();
        
        
        PriorityQueue<Nodo> queue = new PriorityQueue<>(new Nodo());
        List<int[][]> ruta = new LinkedList<>();
        Heuristica heuristica = new Heuristica();
        Nodo nodo = new Nodo(puzzle.getEstado(),heuristica.calcularHeuristica(puzzle.getEstado(), estadoFinal, 3));
        queue.add(nodo);

        //Hashmap de visitados
        HashMap<List<Integer>, Boolean> visitado = new HashMap<>();
        
        //Hashmap de padres para calcular distancia hacia el origen
        HashMap<List<Integer>,List<Integer>> padres = new HashMap<>();
        
        //Marcar el nodo padre como padre
        int[][] origen = puzzle.getEstado();
        padres.put(MatrizUtil.aplanar(origen), MatrizUtil.aplanar(MatrizUtil.generarMatriz(-1)));
        
        while(!queue.isEmpty()) {
            Nodo padre = queue.poll();
            puzzle.setEstado(padre.getEstado());
            if (puzzle.haGanado(estadoFinal)) {
                encuentraSolucion = true;
                resultado = MapUtil.listaHaciaPadre(padre.getEstado(),origen, padres);
                break; 
            }
            else {
                List<int[][]> hijos = puzzle.getEstadosHijos();
                for (int[][] estado : hijos) {
                    List<Integer> estadoPlano = MatrizUtil.aplanar(estado);
                    visitado.putIfAbsent(estadoPlano, Boolean.FALSE);
                    if (Objects.equals(visitado.get(estadoPlano), Boolean.FALSE)) {
                        
                        //Agregar este estado como hijo del padre                        
                        padres.put(MatrizUtil.aplanar(estado), MatrizUtil.aplanar(padre.getEstado()));
                        
                        visitado.put(MatrizUtil.aplanar(estado), Boolean.TRUE);    
                        Integer heuristicaTotal = heuristica.calcularHeuristica(estado, estadoFinal, 3);
                        queue.add(new Nodo(estado, heuristicaTotal));
                        nodosEnRam += MemoryMeasurer.measureBytes(new Nodo(estado, heuristicaTotal));                        
                    }                    
                }
            }
            nodosGenerados++;
        }
        tiempo = (System.nanoTime() - tiempoInicio)/1000000.0;
    }      
    @Override
    public Reporte getReporte() {
        Reporte reporte = new Reporte();
        reporte.setAlgoritmoUsado("Greedy Best First Search");
        reporte.setEncontroSolucion(encuentraSolucion ? "S�" : "No");
        reporte.setEstadoFinal(MatrizUtil.aplanar(estadoFinal).toString());
        reporte.setEstadoInicial(MatrizUtil.aplanar(estadoInicial).toString());
        reporte.setNodosGenerados(nodosGenerados.toString());
        reporte.setNodosGeneradosEnRam("0");
        reporte.setResultado(resultado);
        reporte.setTiempo(tiempo.toString());
        reporte.setNodosGeneradosEnRam(nodosEnRam.toString());
        Integer movidas = resultado.size() - 1;
        reporte.setNumeroMovidas(movidas.toString());
        return reporte;
    }
}
