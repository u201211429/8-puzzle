/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia_201502.algoritmos;

/**
 *
 * @author jamil
 */
public class Heuristica {

    public Heuristica() {
    }
    
    public int calcularHeuristica(int[][] estado, int[][] estadoFinal, int size) {
        int columnaDestino = 0;
        int filaDestino = 0;
        int heuristicaTotal = 0;
        int[] destinos = new int[2];
        
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                destinos = obtenerPosicionDestino(estado, estadoFinal, i, j, size);
                columnaDestino = destinos[0];
                filaDestino = destinos[1];
                heuristicaTotal += distanciaManhattan(i, j, columnaDestino, filaDestino);                                
            }
        }
        return heuristicaTotal;
    }
    
    public int distanciaManhattan(int columnaOrigen, int filaOrigen, int columnaDestino, int filaDestino) {
        return Math.abs(columnaOrigen - columnaDestino) + Math.abs(filaOrigen - filaDestino);
    }
    
    private int[] obtenerPosicionDestino(int[][] estadoInicial, int[][] estadoFinal, int columnaOrigen, int filaOrigen, int size) {
        int numero = estadoInicial[columnaOrigen][filaOrigen];
        for (int i = 0; i<size; i++){ 
            for (int j = 0; j<size; j++) {
                if (numero == estadoFinal[i][j]) {
                    int[] destino = {i,j};
                    return destino;
                }
            }
        }
        return null;
    }    
}
