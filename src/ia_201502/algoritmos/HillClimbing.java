package ia_201502.algoritmos;

import ia_201502.estructuras.Reporte;
import ia_201502.estructuras.Nodo;
import ia_201502.puzzle.Puzzle;
import ia_201502.util.MapUtil;
import ia_201502.util.MatrizUtil;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import objectexplorer.MemoryMeasurer;

public class HillClimbing implements Algoritmo {
        
    Long nodosEnRam = new Long(0);   
    Integer nodosGenerados = 0;
    boolean encuentraSolucion = false;
    Double tiempo = 0.0;
    int[][] estadoFinal;
    int[][] estadoInicial;
    Puzzle puzzle;
    List<List<Integer>> resultado;
    
    public HillClimbing(Puzzle puzzle, int[][] estadoFinal) {
        this.estadoFinal = estadoFinal;
        this.estadoInicial = puzzle.getEstado();
        this.puzzle = puzzle;
    }

    @Override
    public void correr() {
        nodosEnRam += MemoryMeasurer.measureBytes(new Nodo(puzzle.getEstado(), 1));
        long tiempoInicio = System.nanoTime();
        
        int[][] estadoInicial = puzzle.getEstado();

        int menorHeuristica, heuristicaPadre = 1;

        PriorityQueue<Nodo> heuristicasHijos = new PriorityQueue<>(new Nodo());
        List<int[][]> hijos;

        int[][] hijoMenor = null;

        boolean cortar = false;

        Heuristica heuristica = new Heuristica();

        //Hashmap de padres para calcular distancia hacia el origen
        HashMap<List<Integer>, List<Integer>> padres = new HashMap<>();

        padres.put(MatrizUtil.aplanar(estadoInicial), MatrizUtil.aplanar(estadoInicial));

        while (heuristicaPadre != 0 && cortar != true) {
            nodosGenerados++;
            heuristicaPadre = heuristica.calcularHeuristica(puzzle.getEstado(), estadoFinal, 3);
            hijos = puzzle.getEstadosHijos();

            heuristicasHijos.clear();

            for (int[][] hijo : hijos) {
                heuristicasHijos.add(new Nodo(hijo, heuristica.calcularHeuristica(hijo, estadoFinal, 3)));
            }

            Nodo mejor = heuristicasHijos.poll();

            menorHeuristica = mejor.getHeuristica();
            hijoMenor = mejor.getEstado();

            padres.put(MatrizUtil.aplanar(hijoMenor), MatrizUtil.aplanar(puzzle.getEstado()));

            if (menorHeuristica < heuristicaPadre) {

                heuristicaPadre = menorHeuristica;

                puzzle.setEstado(hijoMenor);

            } else {
                cortar = true;
            }
        }
        if (heuristicaPadre == 0) {
            encuentraSolucion = true;
        }
        else {
            encuentraSolucion = false;
        }
        
        tiempo = (System.nanoTime() - tiempoInicio)/1000000.0;        
        resultado =  MapUtil.listaHaciaPadre(hijoMenor, estadoInicial, padres);

    }

    @Override
    public Reporte  getReporte() {
        Reporte reporte = new Reporte();
        reporte.setAlgoritmoUsado("Hill Climbing");
        reporte.setEncontroSolucion(encuentraSolucion ? "S�" : "No");
        reporte.setEstadoFinal(MatrizUtil.aplanar(estadoFinal).toString());
        reporte.setEstadoInicial(MatrizUtil.aplanar(estadoInicial).toString());
        reporte.setNodosGenerados(nodosGenerados.toString());
        reporte.setNodosGeneradosEnRam(nodosEnRam.toString());
        reporte.setResultado(resultado);
        reporte.setTiempo(tiempo.toString());
        Integer movidas = resultado.size() - 1;
        reporte.setNumeroMovidas(movidas.toString());
        return reporte;
    }
}
