/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia_201502.algoritmos;

import ia_201502.estructuras.Reporte;
import ia_201502.puzzle.Puzzle;
import ia_201502.util.MatrizUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Jamil
 */
public class SimulatedAnnealing implements Algoritmo {

    Long nodosEnRam = new Long(0);
    Integer nodosGenerados = 0;
    boolean encuentraSolucion = false;
    Double tiempo = 0.0;
    int[][] estadoFinal;
    int[][] estadoInicial;
    Puzzle puzzle;
    List<List<Integer>> resultado;

    public SimulatedAnnealing(Puzzle puzzle, int[][] estadoFinal) {
        this.estadoFinal = estadoFinal;
        this.estadoInicial = puzzle.getEstado();
        this.puzzle = puzzle;
    }

    private double schedule(int time, int MAX_TIME, int MAX_TEMP) {
        double timed = (double) time / MAX_TIME;
	return MAX_TEMP * timed*(timed-2) + MAX_TEMP ;
    }

    @Override
    public void correr() {
        resultado = new ArrayList<>();
        Random random = new Random();
        Heuristica heuristica = new Heuristica();
        double temperatura = 0;
        int heuristicaActual;
        int[][] mejorEstado = puzzle.getEstado();

        int MAX_TIME = 50000;

        for (int i = 0; i < MAX_TIME; i++) {
            temperatura = schedule(i, MAX_TIME-1, 10000);

            heuristicaActual = heuristica.calcularHeuristica(puzzle.getEstado(), estadoFinal, 3);
            List<int[][]> hijos = puzzle.getEstadosHijos();

            int[][] hijoEscogido = hijos.get(random.nextInt(hijos.size()));
            int heuristicaSiguiente = heuristica.calcularHeuristica(hijoEscogido, estadoFinal, 3);

            if (probabilidadDeAceptacion(heuristicaActual, heuristicaSiguiente, temperatura) > Math.random()) {
                puzzle.setEstado(hijoEscogido);
                
            }

            if (heuristica.calcularHeuristica(hijoEscogido, estadoFinal, 3)
                    < heuristica.calcularHeuristica(mejorEstado, estadoFinal, 3)) {
                mejorEstado = hijoEscogido;
            }
        }
        int x = heuristica.calcularHeuristica(mejorEstado, estadoFinal, 3);
    }
    

    @Override
    public Reporte getReporte() {
        return new Reporte();
    }

    private double probabilidadDeAceptacion(int heuristicaActual, int heuristicaSiguiente, double temperatura) {
        if (heuristicaSiguiente < heuristicaActual) {
            return 1.0;
        }
        return Math.exp((heuristicaActual - heuristicaSiguiente) / temperatura);
    }
}
