/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia_201502.estructuras;

import java.util.List;

/**
 *
 * @author jamil
 */
public class Reporte {

    String algoritmoUsado;
    String estadoInicial;
    String estadoFinal;
    String encontroSolucion;
    String NodosGenerados;
    String nodosGeneradosEnRam;
    String tiempo;
    String numeroMovidas;
    List<List<Integer>> resultado;

    public Reporte() {
    }

    public Reporte(String algoritmoUsado, String estadoInicial, String estadoFinal, String encontroSolucion, String NodosGenerados, String nodosGeneradosEnRam, String tiempo, String numeroMovidas, List<List<Integer>> resultado) {
        this.algoritmoUsado = algoritmoUsado;
        this.estadoInicial = estadoInicial;
        this.estadoFinal = estadoFinal;
        this.encontroSolucion = encontroSolucion;
        this.NodosGenerados = NodosGenerados;
        this.nodosGeneradosEnRam = nodosGeneradosEnRam;
        this.tiempo = tiempo;
        this.numeroMovidas = numeroMovidas;
        this.resultado = resultado;
    }

    public String getAlgoritmoUsado() {
        return algoritmoUsado;
    }

    public void setAlgoritmoUsado(String algoritmoUsado) {
        this.algoritmoUsado = algoritmoUsado;
    }

    public String getEstadoInicial() {
        return estadoInicial;
    }

    public void setEstadoInicial(String estadoInicial) {
        this.estadoInicial = estadoInicial;
    }

    public String getEstadoFinal() {
        return estadoFinal;
    }

    public void setEstadoFinal(String estadoFinal) {
        this.estadoFinal = estadoFinal;
    }

    public String getEncontroSolucion() {
        return encontroSolucion;
    }

    public void setEncontroSolucion(String encontroSolucion) {
        this.encontroSolucion = encontroSolucion;
    }

    public String getNodosGenerados() {
        return NodosGenerados;
    }

    public void setNodosGenerados(String NodosGenerados) {
        this.NodosGenerados = NodosGenerados;
    }

    public String getNodosGeneradosEnRam() {
        return nodosGeneradosEnRam;
    }

    public void setNodosGeneradosEnRam(String nodosGeneradosEnRam) {
        this.nodosGeneradosEnRam = nodosGeneradosEnRam;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public List<List<Integer>> getResultado() {
        return resultado;
    }

    public void setResultado(List<List<Integer>> resultado) {
        this.resultado = resultado;
    }

    public String getNumeroMovidas() {
        return numeroMovidas;
    }

    public void setNumeroMovidas(String numeroMovidas) {
        this.numeroMovidas = numeroMovidas;
    }    
}
