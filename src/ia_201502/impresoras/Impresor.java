package ia_201502.impresoras;

import ia_201502.estructuras.Reporte;
import ia_201502.algoritmos.Algoritmo;
import java.util.List;

public interface Impresor {
    public void reportar(Reporte reporte);
}
