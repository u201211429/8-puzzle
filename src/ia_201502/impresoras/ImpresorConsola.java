package ia_201502.impresoras;

import ia_201502.estructuras.Reporte;
import ia_201502.algoritmos.Algoritmo;
import ia_201502.util.MatrizUtil;
import java.util.List;

public class ImpresorConsola implements Impresor {

    public ImpresorConsola(){  
    }

    @Override
    public void reportar(Reporte reporte) {
        System.out.println("---------------------------------");
        System.out.println("Problema: 8-puzzle");
        System.out.println("Algoritmo usado: " + reporte.getAlgoritmoUsado());
        System.out.println("Estado inicial: " + reporte.getEstadoInicial());
        System.out.println("Estado final: " + reporte.getEstadoFinal());
        System.out.println("�Encuentra soluci�n?: " + reporte.getEncontroSolucion());
        System.out.println("N�mero de Movidas: " + reporte.getNumeroMovidas());
        System.out.println("Nro. nodos generados: " + reporte.getNodosGenerados());
        System.out.println("Nro. nodos en RAM: " + reporte.getNodosGeneradosEnRam() + " (Bytes)");
        System.out.println("Tiempo (milisegundos): " + reporte.getTiempo());
        
        
        //Para no incluir el estado inicial que no cuenta como movimeinto
        //int movimientos = resultado.size() - 1;
    }        
}
