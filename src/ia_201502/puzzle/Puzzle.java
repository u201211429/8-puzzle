package ia_201502.puzzle;

import ia_201502.util.MatrizUtil;
import java.util.ArrayList;
import java.util.List;

public class Puzzle {

    int[][] estado;

    public Puzzle() {
        estado = new int[3][3];
    }

    public int[] posicionDeCero() {
        int fila = 0, columna = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (estado[i][j] == 0) {
                    fila = i;
                    columna = j;
                }
            }
        }

        int[] posicion = new int[2];
        posicion[0] = fila;
        posicion[1] = columna;
        return posicion;
    }

    public int[][] moverIzquierda() {
        int[][] matrizTemporal = MatrizUtil.copiarMatriz(estado, 3);
        int[] posicionCero = posicionDeCero();
        int fila = posicionCero[0];
        int columna = posicionCero[1];

        if (columna - 1 < 0) {
            MatrizUtil.errarMatriz(matrizTemporal);
            return matrizTemporal;
        } else {
            int aux = matrizTemporal[fila][columna - 1];
            matrizTemporal[fila][columna - 1] = matrizTemporal[fila][columna];
            matrizTemporal[fila][columna] = aux;
            
        }
        return matrizTemporal;
    }
    
    public int[][] moverDerecha() {
        int[][] matrizTemporal = MatrizUtil.copiarMatriz(estado, 3);
        int[] posicionCero = posicionDeCero();
        int fila = posicionCero[0];
        int columna = posicionCero[1];

        if (columna + 1 >= 3) {
            MatrizUtil.errarMatriz(matrizTemporal);
            return matrizTemporal;
        } else {
            int aux = matrizTemporal[fila][columna + 1];
            matrizTemporal[fila][columna + 1] = matrizTemporal[fila][columna];
            matrizTemporal[fila][columna] = aux;            
        }
        return matrizTemporal;
    }
    
    public int[][] moverArriba() {
        int[][] matrizTemporal = MatrizUtil.copiarMatriz(estado, 3);        
        int[] posicionCero = posicionDeCero();
        int fila = posicionCero[0];
        int columna = posicionCero[1];

        if (fila - 1 < 0) {
            MatrizUtil.errarMatriz(matrizTemporal);
            return matrizTemporal;
        } else {
            int aux = matrizTemporal[fila - 1][columna];
            matrizTemporal[fila - 1][columna] = matrizTemporal[fila][columna];
            matrizTemporal[fila][columna] = aux;            
        }
        return matrizTemporal;
    }
    
    public int[][] moverAbajo() {
        int[][] matrizTemporal = MatrizUtil.copiarMatriz(estado, 3);        
        int[] posicionCero = posicionDeCero();
        int fila = posicionCero[0];
        int columna = posicionCero[1];

        if (fila + 1 >= 3) {
            MatrizUtil.errarMatriz(matrizTemporal);
            return matrizTemporal;
        } else {
            int aux = matrizTemporal[fila + 1][columna];
            matrizTemporal[fila + 1][columna] = matrizTemporal[fila][columna];
            matrizTemporal[fila][columna] = aux;            
        }
        return matrizTemporal;
    }
    
    public List<int[][]> getEstadosHijos() {
        List<int[][]> hijos = new ArrayList<>();
        int[][] matrizTemporal = new int[3][3];
        
        matrizTemporal = moverAbajo();
        if(!MatrizUtil.esMatrizErrada(matrizTemporal)) {
            hijos.add(MatrizUtil.copiarMatriz(matrizTemporal,3));
        }
        
        matrizTemporal = moverArriba();
        
        if (!MatrizUtil.esMatrizErrada(matrizTemporal)) {
            hijos.add(MatrizUtil.copiarMatriz(matrizTemporal,3));
        }
        
        matrizTemporal = moverDerecha();
        
        if (!MatrizUtil.esMatrizErrada(matrizTemporal)) {
            hijos.add(MatrizUtil.copiarMatriz(matrizTemporal,3));
        }
        
        matrizTemporal = moverIzquierda();
        
        if (!MatrizUtil.esMatrizErrada(matrizTemporal)) {
            hijos.add(MatrizUtil.copiarMatriz(matrizTemporal,3));
        }
        return hijos;
    }   
    
    public boolean haGanado(int[][] estadoFinal)
    {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (estadoFinal[i][j] != estado[i][j]) {
                    return false;
                }
            }
        }                
        return true;
    }
    
    public boolean validarResolvible(int[][] estado) {
       
        //Algoritmo de inversiones, sirve para hallar si se puede resolver el 8 puzzle
        int inversiones = 0;
        List<Integer> estadoPlano = MatrizUtil.aplanar(estado);
        
        for (int i = 0; i<estadoPlano.size(); i++) {
            if (estadoPlano.get(i) == 0) {
                estadoPlano.remove(i);
            }            
        }
        
        for (int i = 0; i<estadoPlano.size(); i++) {
            for (int j = i + 1; j<estadoPlano.size(); j++ ) {
                if (estadoPlano.get(j) < estadoPlano.get(i)) {
                    inversiones++;
                }                
            }                
        }
        if (inversiones % 2 == 0) {
            return true;
        }
        return false;
    }

    public int[][] getEstado() {
        return MatrizUtil.copiarMatriz(estado,3);
    }

    public void setEstado(int[][] estado) {
        this.estado = estado;
    }
}
