/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ia_201502.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.omg.CORBA.INTERNAL;

/**
 *
 * @author jamil
 */
public class MatrizUtil {

    public static int[][] copiarMatriz(int[][] origen, int size) {
        int[][] destino = new int[3][3];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                destino[i][j] = origen[i][j];
            }
        }
        return destino;
    }
    

    public static void errarMatriz(int[][] matriz) {
        matriz[0][0] = -1;
    }

    public static boolean esMatrizErrada(int[][] matriz) {
        if (matriz[0][0] == -1) {
            return true;
        }
        return false;
    }

    public static void imprimirMatriz(int[][] matriz) {
        for (int[] fila : matriz) {
            for (int numero : fila) {
                System.out.print(numero);
            }
            System.out.println("");
        }
        System.out.println("");
    }
    
    public static boolean matrizEsIgual(int[][] matrizA, int[][] matrizB) {
        for (int i = 0; i<3 ;i++)        {
            for (int j = 0; j<3; j++) {
                if (matrizA[i][j] != matrizB[i][j]) {
                    return false;
                }
            }
            
        }
        return true;
    }
    
    public static int[][] generarMatriz(int numeroParaLlenar) {
        int[][] matriz = new int[3][3];
        for (int i = 0; i<3 ;i++)        {
            for (int j = 0; j<3; j++) {
                matriz[i][j] = numeroParaLlenar;
            }
        }
        return matriz;
    }
    
    public static List<Integer> aplanar(int[][] matriz) {
        List<Integer> arreglo = new ArrayList<>();
        for(int[] fila : matriz) {
            for (int numero : fila) {
                arreglo.add(numero);
            }
        }
        return arreglo;
    }
    
    public static int[][] agordar(List<Integer> lista) {
        int counter = 0;
        int[][] matriz = new int[3][3];
        for (int i = 0; i<3 ;i++)        {
            for (int j = 0; j<3; j++) {
                matriz[i][j] = lista.get(counter);
                counter++;
            }
        }
        return matriz;
    }
}
